# Software & Hardware - Dokumentation am FvS

## Create, Share and Collaborate

Auf dieser Seite kannst du die häufigsten Fragen, Fehler und Tipps nachlesen, die es zu unseren Geräten und abgesprochenen Anwendungen gibt. Wenn du dich selbst registriert besteht auch die Möglichkeit, selbst einen Änderungsvorschlag für ein Dokument zur versenden, welcher dann angenommen oder abgelehnt werden kann. So können wir uns Arbeit aufteilen und Fehler in der Dokumentation ausbessern.